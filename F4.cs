using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp7
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double v;
            double p;
            if (textBox1.Text == "")
            {
                v = 0;
            }
            else
            {
                v = double.Parse(textBox1.Text);
            }
            p = 250 + (0.057 * v);
            MessageBox.Show("su pago de la semana será de: " + p,"Pago");
            Close();
        }
    }
}