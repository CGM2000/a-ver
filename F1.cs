using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp7
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MessageBox.Show(" Bienvenido, continue y calcule su pago de la semana.", "Reciba un cordial saludo");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(" Seleccionó Administrador", "Codigo 1");
            Form2 f = new Form2();
            f.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(" Seleccionó Empleado por hora", "Codigo 2");
            Form3 F = new Form3();
            F.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show(" Seleccionó Empleado por comisión", "Codigo 4");
            Form4 d = new Form4();
            d.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show(" Seleccionó Empleado por destajo", "Codigo 3");
            Form5 D = new Form5();
            D.Show();
        }
    }
}