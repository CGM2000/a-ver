using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp7
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }
        double s, x, h;

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        { 
            if ((textBox1.Text == "") || (textBox3.Text == ""))
            {
                MessageBox.Show("Aun no ha ingresado las horas trabajadas o el salario por hora", "Error");
            }
            else
            {
                if (textBox2.Text == "")
                {
                    x = 0;
                }
                else
                {
                    x = double.Parse(textBox2.Text);
                }
                h = double.Parse(textBox1.Text);
                s = double.Parse(textBox3.Text);
                s = (s*h) + ((s)*(x) * (1.5));
                MessageBox.Show("Su pago de la semana será de: " + s, "Pago");
                Close();
            }
        }
    }
}


