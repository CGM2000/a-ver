using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp7
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double a, b;
            if ((textBox1.Text == "") || (textBox2.Text == ""))
            {
                MessageBox.Show("Aun no ha ingresado el monto por pieza o la cantidad de piezas producidas en la semana","Error");
            }
            else
            {
                a = double.Parse(textBox1.Text);
                b = double.Parse(textBox2.Text);
                a = a * b;
                MessageBox.Show(" Su pago de la semana será de: " + a, "Pago");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
